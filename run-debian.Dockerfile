arg DEBIAN_VERSION=${DEBIAN_VERSION:-stable}

from debian:${DEBIAN_VERSION}-slim

run apt-get -qq update \
  && apt-get -qq install \
  libunwind8 \
  libgc1 \
  libre2-9 \
  && apt-get -qq autoclean \
  && apt-get -qq autoremove \
  && rm -rf /var/lib/apt/lists/*
