arg DEBIAN_VERSION=${DEBIAN_VERSION:-stable}

from debian:${DEBIAN_VERSION}-slim

arg JDK_VERSION=${JDK_VERSION:-11}
arg SBT_VERSION=${SBT_VERSION:-1.3.10}

run mkdir -p /usr/share/man/man1 \
  && apt-get -qq update \
  && apt-get -qq install \
  clang \
  libunwind-dev \
  libgc-dev \
  libre2-dev \
  curl \
  openjdk-${JDK_VERSION}-jre-headless \
  && curl -sSL -o sbt.tgz https://github.com/sbt/sbt/releases/download/v${SBT_VERSION}/sbt-${SBT_VERSION}.tgz \
  && tar -xvzf sbt.tgz \
  && rm sbt.tgz \
  && ln -s /sbt/bin/sbt /usr/bin/sbt \
  && apt-get -qq autoremove curl \
  && apt-get -qq clean \
  && rm -rf /var/lib/apt/lists/* \
  && sbt sbtVersion -Dsbt.rootdir=true

entrypoint ["sbt"]
