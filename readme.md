# SBT Docker Images

[![pipeline status](https://gitlab.com/druidgreeneyes/sbt-docker/badges/master/pipeline.svg)](https://gitlab.com/druidgreeneyes/sbt-docker/-/commits/master)

Up to date, built directly from a base image instead of going through multiple layers of indirection, and ready for scala-native.

## Usage

For scala-jvm projects, just use the `build-alpine` or `build-debian` image to produce a jar, then deploy that jar any way you like.

For scala-native projects, use the `build-alpine` or `build-debian` image to produce a native binary, then use the corresponding `run` image to host that binary:

```Dockerfile
from druidgreeneyes/sbt:build-alpine as build

workdir /build

copy . .

run sbt "compile; nativeLink"

from druidgreeneyes/sbt:run-alpine as run

copy --from=build \
  /build/target/scala-3.2.2/build-out \
  /your-binary-name

entrypoint ["/your-binary-name"]
```

Note that if you need to link against other libraries beyond those provided by default (`unwind`, `re2`, `gc`) you may need to install them in both images as part of your build process.

## Source

<https://gitlab.com/druidgreeneyes/sbt-docker>
