arg ALPINE_VERSION=${ALPINE_VERSION:-3.11}

from alpine:${ALPINE_VERSION}

run apk add --no-cache \
  libunwind \
  gc \
  re2 \
