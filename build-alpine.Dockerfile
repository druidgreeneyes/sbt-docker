arg ALPINE_VERSION=${ALPINE_VERSION:-3.11}

from alpine:${ALPINE_VERSION}

arg JDK_VERSION=${JDK_VERSION:-11}
arg SBT_VERSION=${SBT_VERSION:-1.3.10}

run apk --no-cache add \
  clang \
  libc-dev \
  libunwind-dev \
  gc-dev \
  re2-dev \
  musl-dev \
  build-base \
  bash \
  curl \
  openjdk${JDK_VERSION}-jre-headless \
  && curl -sSL -o sbt.tgz https://github.com/sbt/sbt/releases/download/v${SBT_VERSION}/sbt-${SBT_VERSION}.tgz \
  && tar -xvzf sbt.tgz \
  && rm sbt.tgz \
  && ln -s /sbt/bin/sbt /usr/bin/sbt \
  && apk --no-cache del curl \
  && sbt sbtVersion -Dsbt.rootdir=true

entrypoint ["sbt"]
